<pre><code>SomaFM Python player >>></code></pre>


Make executable if needed. And put in Home dir ::

#### `$ chmod +x somafm.py`

Run SFM ::


#### `$ python3 somafm.py -l` = somafm list of channels
- or another python version

##### My aliases ::
- alias sonic='python3 somafm.py sonic'	#sfml
- alias def='python3 somafm.py  def'	#sfml
- alias dub='python3 somafm.py  dub'	#sfml
- alias heavy='python3 somafm.py  heavy'	#sfml
- alias space='python3 somafm.py  space'	#sfml
- alias secret='python3 somafm.py  secret'	#sfml
- alias groove='python3 somafm.py  groove'	#sfml
- alias special='python3 somafm.py  somafm'	#sfml
- alias mission='python3 somafm.py  mission'	#sfml

![Schermafdruk_2020-11-11_10-14-44](/uploads/2c253d1267aac3c02d08ffac7c54ba2e/Schermafdruk_2020-11-11_10-14-44.png)

###### notification desktop personal gtk theme ::green::

![Schermafdruk_2020-11-11_11-13-15](/uploads/d92bf97cca327a7f6f0bfa4525c64aa3/Schermafdruk_2020-11-11_11-13-15.png)

If you like what you hear on SomaFM and want to help, please consider visiting their site and [making a donation](https://somafm.com/support/).
                         
I got this from github long time ago :: but wanted a backup on gitlab for myself :: 

#### Go to the original github page for the latest update :: 
##### - https://github.com/MS3FGX/SomaFM